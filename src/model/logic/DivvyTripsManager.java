package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import model.data_structures.IDoubleLinkedList;
import model.data_structures.Node;

public class DivvyTripsManager implements IDivvyTripsManager {

	
	public void loadStations (String stationsFile) throws IOException
	{
		File stations = new File("data/Divvy_Stations_2017_Q3Q4");

			FileReader reader = new FileReader(stations);
			BufferedReader lector = new BufferedReader(reader);
			
			String linea=lector.readLine();
			while(linea!=null)
			{
				String[] contenido;
				contenido = linea.split(",");
				int total = contenido.length;
				Node nodo = new Node(total);
				for(int i=0; i<total;i++)
				{
					nodo.add(contenido[i]);
				}
				linea=lector.readLine();
			}
			lector.close();
			reader.close();
	}

	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public IDoubleLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IDoubleLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		return null;
	}	


}
