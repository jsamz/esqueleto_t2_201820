package model.data_structures;

import model.data_structures.Node;
/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoubleLinkedList<T extends Comparable<T>> extends Iterable<T> 
{

	Integer getSize();
	void add(T pObj );
	void addAtEnd(T pObj);
	void addAtK(T pObj, int pP);
	T getElement(int pP);
	T getCurrentElement();
	//Elimina el nodo actual
	void delete();
	void deleteAtK(int pP);
	T next();
	T previous();

}
