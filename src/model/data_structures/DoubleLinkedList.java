package model.data_structures;

import java.util.Iterator;
import model.data_structures.Node;

public class DoubleLinkedList<DoubleLinkedList extends Comparable<Node>> implements IDoubleLinkedList
{
	
	//Atributos
	
	private int size = 10;
	private int actualSize = 0;
	private int actualNode = 0;
	private Node[] nodos;
	
	public DoubleLinkedList(int pSize)
	{
		size = pSize;
		nodos = new Node[size];
	}


	@Override
	public Integer getSize()
	{
		return actualSize;
	}

	@Override
	public void add(Comparable pObj) 
	{
		 if ( actualSize == size )
		 { // caso de arreglo lleno (aumentar tamaNo)
		 	size = 2 * size;
		 	Node[ ] copia = nodos;
		 	nodos = (Node [ ]) new Comparable[size];
		 	nodos[0] = (Node) pObj;
		 	for ( int i = 0; i < actualSize; i++)
		 	{
		 		nodos[i+1] = copia[i];
		 	}
		 }
		 else
		 {
			 Node[ ] copia = nodos;
			 nodos = (Node [ ]) new Comparable[size];
			 nodos[0] = (Node) pObj;
		 	for ( int i = 0; i < actualSize; i++)
		 	{
		 		nodos[i+1] = copia[i];
		 	}
		 }
		 actualSize++;

	}

	@Override
	public void addAtEnd(Comparable pObj) 
	{
		 if ( actualSize == size )
		 { // caso de arreglo lleno (aumentar tamaNo)
			 size = 2 * size;
			 Node[ ] copia = nodos;
			 nodos = (Node [ ]) new Comparable[size];
			 for ( int i = 0; i < actualSize; i++)
			 {
				 nodos[i] = copia[i];
			 }
		 }
		 nodos[actualSize] = (Node) pObj;
		 actualSize++;
	}

	@Override
	public void addAtK(Comparable pObj, int pP) 
	{
		boolean done = false;
		 if ( actualSize == size )
		 { // caso de arreglo lleno (aumentar tamaNo)
		 	size = 2 * size;
		 	Node[ ] copia = nodos;
		 	nodos = (Node [ ]) new Comparable[size];
		 	for ( int i = 0; i < actualSize; i++)
		 	{
		 		if(done)
		 		{
		 		nodos[i] = copia[i-1];
		 		}
		 		if(i==pP)
		 		{
		 		nodos[i] = (Node) pObj;
		 		done = true;
		 		}
		 		else
		 		{	
			 	nodos[i] = copia[i];
		 		}
		 	}
		 }
		 else
		 {
			 Node[ ] copia = nodos;
			 nodos = (Node [ ]) new Comparable[size];
			 	for ( int i = 0; i < actualSize; i++)
			 	{
			 		if(done)
			 		{
			 		nodos[i] = copia[i-1];
			 		}
			 		if(i==pP)
			 		{
			 		nodos[i] = (Node) pObj;
			 		done = true;
			 		}
			 		else
			 		{	
				 	nodos[i] = copia[i];
			 		}
			 	}
		 }
		 actualSize++;

		
	}

	@Override
	public Comparable<Node> getElement(int pP) 
	{
		Comparable<Node> nodo = null;
		boolean encontrado = false;
	 	for ( int i = 0; i < actualSize && encontrado==false; i++)
	 	{
	 		if(i==pP)
	 		{
	 		nodo = (Comparable<Node>) nodos[i];
	 		encontrado=true;
	 		}
	 	}
	 	return nodo;
	}

	@Override
	public Comparable<Node> getCurrentElement() 
	{
		return (Comparable<Node>) nodos[actualNode];
	}

	@Override
	public void delete() 
	{
		boolean done = false;
		 Node[ ] copia = nodos;
		 nodos = (Node [ ]) new Comparable[size];
		 	for ( int i = 0; i < actualSize; i++)
		 	{
		 		if(done)
		 		{
		 		nodos[i] = copia[i+1];
		 		}
		 		if(i==actualNode)
		 		{
		 		nodos[i] = copia[i+1];
		 		done = true;
		 		}
		 		else
		 		{
		 		nodos[i] = copia[i];	
		 		}
		 	}
	}

	@Override
	public void deleteAtK(int pP) {
		boolean done = false;
		 Node[ ] copia = nodos;
		 nodos = (Node [ ]) new Comparable[size];
		 	for ( int i = 0; i < actualSize; i++)
		 	{
		 		if(done)
		 		{
		 		nodos[i] = copia[i+1];
		 		}
		 		if(i==pP)
		 		{
		 		nodos[i] = copia[i+1];
		 		done = true;
		 		}
		 		else
		 		{
		 		nodos[i] = copia[i];	
		 		}
		 	}
	}

	@Override
	public Comparable next() {
		actualNode++;
		return (Comparable) nodos[actualNode];
	}

	@Override
	public Comparable previous() {
		actualNode--;
		return (Comparable) nodos[actualNode];
	}


	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}


}